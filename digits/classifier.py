import csv
import atexit
import numpy as np
import re
from perceptron import *

perceptrons = []

''' HELPER FUNCTIONS '''

def mira(correct, incorrect, features):
    '''
    Returns the tau term in performing MIRA (Minimum Infused Relaxed Algorithm)
    '''
    difference = [x - y for x, y in zip(incorrect, correct)]
    return (np.dot(difference, features) + 1.)/(np.dot(map(lambda x: 2 * x, features), features))

''' CORE FUNCTIONS '''

def train():
    with open('train.csv', 'r') as training_data:
        csvfile = csv.reader(training_data)
        label = csvfile.next()
        counter = 0

        for row in csvfile:
            counter += 1
            if counter == 1000: break

            digit = int(row[0])
            row = map(int, row[1:])

            for perceptron in perceptrons:
                classification = perceptron.classify(perceptron.genFeature(row))
                if digit != classification:
                    tau = mira(perceptron.getWeights()[digit], perceptron.getWeights()[classification], row)
                    perceptron.changeWeightVector(digit, map(lambda x: tau * x, row))
                    perceptron.changeWeightVector(classification, map(lambda x: -tau * x, row))

def classify():
    with open('train.csv', 'r') as training_data:
        csvfile = csv.reader(training_data)
        label = csvfile.next()
        counter = 0

        correct, total = 0, 0

        for row in csvfile:
            counter += 1
            if counter == 1000: break

            digit = int(row[0])
            row = map(int, row[1:])

            for perceptron in perceptrons:
                classification = perceptron.classify(perceptron.genFeature(row))
                if digit == classification: correct += 1
                total += 1

        print "Correct: {0} out of {1}".format(correct, total)

''' SETUP '''

def interactive():
    print "Type a command. Enter 'help' for options"
    while True:
        cmd = raw_input()
        cmd_train = re.compile(r'^train$')
        cmd_classify = re.compile(r'^classify$')
        cmd_weights = re.compile(r'^weights$')
        cmd_help = re.compile(r'^help$')
        cmd_exit = re.compile(r'^exit$')

        if cmd_train.match(cmd):
            train()
            print "Training complete"
        elif cmd_classify.match(cmd):
            classify()
        elif cmd_weights.match(cmd):
            print perceptrons[0].getWeights()
        elif cmd_help.match(cmd):
            print "Train: train\nClassify: classify\nWeights: weights\nHelp: help\nExit: exit\n"
        elif cmd_exit.match(cmd):
            print "Exiting...\n"
            break
        else:
            print "Command '" + cmd + "' not recognized."

def setup():
    nPP = NumPixelPerceptron()
    for i in range(10):
        nPP.appendWeightVector([0] * 784)
    perceptrons.append(nPP)

if __name__ == "__main__":
    setup()
    interactive()