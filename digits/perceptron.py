import numpy as np

class Perceptron:
    '''
    self.weights - An array of weight vectors
    '''

    def __init__(self, weights = []):
        self.weights = weights

    def getWeights(self):
        return self.weights

    def setWeights(self, index, weights):
        if index >= len(self.weights):
            raise PerceptronError("Invalid weight vector")
        self.weights[index] = weights

    def getWeightVector(self, vector):
        return self.weights[vector]

    def appendWeightVector(self, weights = []):
        if weights:
            self.weights.append(weights)

    def changeWeightVector(self, vector, offset):
        self.weights[vector] = np.add(self.weights[vector], offset).tolist()

    def getWeight(self, vector, index):
        return self.weights[vector][index]

    def changeWeight(self, vector, weight, offset):
        if weight >= len(self.weights[vector]):
            raise PerceptronError("Invalid index in weight vector")

        self.weights[vector][weight] += offset

    def classify(self, features, bias=0):
        raise PerceptronError("Classification not implemented")

    def genFeature(self, pixels):
        raise PerceptronError("Feature generation not implemented")

    def __repr__(self):
        reprStr = ""
        for i in range(len(self.weights)):
            reprStr += "Weight vector " + str(i) + ": " + str(self.weights[i]) + "\n"
        return reprStr

class NumPixelPerceptron(Perceptron):
    def classify(self, features, bias = 0):
        '''
        Assumes that the Perceptron has a weight vector for each digit, and returns the respective digit with hightest dot product
        '''
        if len(self.weights) == 0 or len(self.weights[0]) != len(features):
            print len(self.weights[0]), len(features)
            raise PerceptronError("Length of weights does not match length of features")

        sigmoid = lambda z: 1./(1 + np.exp(-z))
        bestDigit, bestDot = 0, 0

        for i in range(len(self.weights)):
            output = sigmoid(np.dot(self.weights[i], features) + bias)
            if output > bestDot:
                bestDot = output
                bestDigit = i

        return bestDigit

    def genFeature(self, pixels):
        '''
        Given raw pixel numbers,
        Returns a binary array representing pixels with a value of 128 or more (indicating darker)
        '''
        return map(lambda x: 0 if x < 128 else 1, pixels)

class PerceptronError(Exception):
    pass